## MusicBox

A touchless music box that plays files when a rfid tag is near to the sensor.

### Details

The box should have a robust wooden case that children can play with.
It should not have any blinking lights or displays, so that music can be 
played without annoying lights during the night.

It will have a RFID sensor built in. If a chipcard or other token comes near 
the sensor and is recognised by the system it will play specified music.

The device will have two onboard speakers for stereo sound.

The device will be driven by a raspberry pi or similar SOC.

The music will be stored locally on an SD-Card and played by MPD.

There will be a daemon running on the device that listens for RFID-Events
and fires control commands for mpd (e.g.: play music/ stop music).

For the case planning the raspberry pi 3 was taken as stated here:
https://www.raspberrypi.org/documentation/hardware/raspberrypi/mechanical/Raspberry-Pi-3B-V1.2-Mechanical.pdf

The design uses 4mm wood.

## Hardware

### Parts list:

1. 1x rapsberry pi3 or piZero

2. 1x hifiberry miniamp
https://www.amazon.de/gp/product/B01NBAY8B0/ref=oh_aui_detailpage_o00_s00?ie=UTF8&psc=1

3. 2x Visaton Speaker Visaton SC 4.7 ND
http://www.visaton.de/en/products/fullrange-systems/sc-47-nd-4-ohm
https://www.amazon.de/gp/product/B003A68NQO/ref=oh_aui_detailpage_o00_s00?ie=UTF8&psc=1

4.  RFID Kit RC522
https://www.amazon.de/gp/product/B01M28JAAZ/ref=oh_aui_detailpage_o03_s00?ie=UTF8&psc=1

## Software

There exist two different versions in this repository:

1. One version using mopidy.
Pro: Can play more than local files (Spotify!!!)
Con: Mopidy is a huge application with many dependencies.

2. One version using mpd
Pro: Acts very fast, is very compact.
Con: Cannot play Spotify.

Depending on your use case you can follow installnotes_mopidy or installnotes_mpd.


