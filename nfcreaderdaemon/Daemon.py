#!/usr/bin/python
from __future__ import unicode_literals, print_function

import time
import sys
import json
import signal
import threading
import RPi.GPIO as GPIO
import MFRC522

from functools import partial
from builtins import input

from mopidy_json_client import MopidyClient
from mopidy_json_client.formatting import print_nice

class NFCStateMachine(object):

    def __init__(self, debug=False):
        super(NFCStateMachine, self).__init__()

        print('Starting NFC Reader for Mopidy')

        # Init variables
        self.state = 'stopped'
        self.uri = None
        self.card = 0
        self.attach_card = 0
        self.save_results = False
        self.debug_flag = debug

        # Instantiate Mopidy Client
        self.mopidy = MopidyClient(
            ws_url='ws://127.0.0.1:6680/mopidy/ws',
            error_handler=self.on_server_error,
            connection_handler=self.on_connection,
            autoconnect=False,
            retry_max=10,
            retry_secs=10
        )

        self.mopidy.debug_client(self.debug_flag)
        self.bind_events()
        self.mopidy.connect()
        self.interval = 1
        self.active_card_counter = 0

        self.thread = threading.Thread(target=self.run, args=())
        self.thread.start()

    def on_connection(self, conn_state):
        if conn_state:
            # Initialize mopidy track and state
            self.state = self.mopidy.playback.get_state(timeout=5)
            tl_track = self.mopidy.playback.get_current_tl_track(timeout=15)
            self.track_playback_started(tl_track)
        else:
            self.state = 'stopped'
            self.uri = None

    def bind_events(self):
        self.mopidy.bind_event('playback_state_changed', self.playback_state_changed)
        self.mopidy.bind_event('stream_title_changed', self.stream_title_changed)
        self.mopidy.bind_event('options_changed', self.options_changed)
        self.mopidy.bind_event('volume_changed', self.volume_changed)
        self.mopidy.bind_event('mute_changed', self.mute_changed)
        self.mopidy.bind_event('seeked', self.seeked)

    # Server Error Handler
    def on_server_error(self, error):
        print_nice('[SERVER_ERROR] ', error, format='error')

    # Mopidy Corelistener Events
    def stream_title_changed(self, title):
        print_nice('> Stream Title: ', title)

    def volume_changed(self, volume):
        print_nice('> Current volume is ', volume, format='volume')

    def playback_state_changed(self, old_state, new_state):
        self.state = new_state
        print_nice('> Playback state changed to ', self.state)

    def mute_changed(self, mute):
        print_nice('> Mute State is ', mute, format='mute')

    def options_changed(self):
        options = {'random': self.mopidy.tracklist.get_random(timeout=10),
                   'single': self.mopidy.tracklist.get_single(timeout=10),
                   'consume': self.mopidy.tracklist.get_consume(timeout=10),
                   'repeat': self.mopidy.tracklist.get_repeat(timeout=10)}
        print_nice('Tracklist Options:', options, format='expand')

    def track_playback_started(self, tl_track):
        track = tl_track.get('track') if tl_track else None
        self.uri = track.get('uri') if track else None
        print_nice('> Current Track: ', track, format='track')

    def seeked(self, time_position):
        print_nice('> Current Position is ', time_position, format='time_position')

    def setup_new_playlist(self, card):
        if card == '1':
            print("Get Card: " + str(card))
            self.pl = self.mopidy.playlists.get_items(uri='place-a-playlist-url-here-copied-from-web-client')
        elif card == '2':
            print("Get Card: " + str(card))
            self.pl = self.mopidy.playlists.get_items(uri='place-a-playlist-url-here-copied-from-web-client')
        for p in self.pl:
            self.mopidy.tracklist.add(uri=p.get('uri', '<None>'))

    def card_signal(self, card):

        # update hold on card counter 
        self.active_card_counter = 5
        # !TBD: stop the decrease thread until the playlist starts
        if self.card != card:
            print("add a specific playlist ... card: " + str(card))
            self.card = card
            self.mopidy.playback.stop()
            self.mopidy.tracklist.clear()
            self.setup_new_playlist(card)
            self.mopidy.playback.play()
        else:
            print("its the same card!")
            if self.state == 'paused':
                self.mopidy.playback.resume()

    def run(self):
        """ Method that runs forever """
        while True:
            # update active card timer
            if self.active_card_counter > 0:
                self.active_card_counter -= 1
            time.sleep(self.interval)


if __name__ == '__main__':

    debug = False
    if len(sys.argv) > 1 and sys.argv[1] == '-v':
        debug = True

    demo = NFCStateMachine(debug=debug)
    # Create an object of the class MFRC522
    MIFAREReader = MFRC522.MFRC522()

    try:
        while True:
            print("Card Counter:", demo.active_card_counter)

            # Scan for cards
            (status,TagType) = MIFAREReader.MFRC522_Request(MIFAREReader.PICC_REQIDL)
            # If a card is found
            if status == MIFAREReader.MI_OK:
                print("Card detected")
                # Get the UID of the card
                (status,uid) = MIFAREReader.MFRC522_Anticoll()
                # If we have the UID, continue
                if status == MIFAREReader.MI_OK:
                    # Print UID
                    print("Card read UID: "+str(uid[0])+","+str(uid[1])+","+str(uid[2])+","+str(uid[3]))
                    uuid = str(uid[0])+str(uid[1])+str(uid[2])+str(uid[3])
                    #this is where your card uuid goes
                    if uuid == "756846131":
                        demo.card_signal("1")
                    if uuid == "2271915328":
                        demo.card_signal("2")

            if demo.active_card_counter == 0 and demo.state != 'paused':
                print("Stopping Playback due to missing Card!")
                demo.mopidy.playback.pause()
            time.sleep(1)
    except KeyboardInterrupt:
        demo.mopidy.playback.stop()
        demo.mopidy.tracklist.clear()
        GPIO.cleanup()
        demo.thread._Thread__stop()
        pass
