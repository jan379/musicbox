#!/usr/bin/env python
# -*- coding: utf8 -*-

import RPi.GPIO as GPIO
import MFRC522
import signal
import logging
import os 
import time
import ConfigParser
import subprocess

config = ConfigParser.RawConfigParser() 
config.read('/etc/musicbox.cfg')

# get configuration for our logging
loglevel = config.get('main', 'debuglevel')
numeric_loglevel = getattr(logging, loglevel.upper(), None)
if not isinstance(numeric_loglevel, int):
        raise ValueError('Invalid log level: %s' % loglevel)
logging.basicConfig(filename='/var/log/musicbox.log', level=numeric_loglevel)

logging.info("Initializing musicbox!")
logging.info("More info can be found at https://blog.blabla.berlin")

continue_reading = True

# Capture SIGINT for cleanup when the script is aborted
def end_read(signal,frame):
    global continue_reading
    logging.info("Ctrl+C captured, ending read.")
    continue_reading = False
    GPIO.cleanup()

# Hook the SIGINT
signal.signal(signal.SIGINT, end_read)



def mapCardToPlaylist(uid):
    try:
        # lookup if uid exists in config file
        playlist = config.get('playlists', uid)
        return playlist
    except:
        logging.info("NFC chip is not mapped in config: " + uid)
        file = '/tmp/unmapped_uids.txt'
        try:
            uidfile = open(file, 'r')
            if not uid in uidfile.read():
                logging.info('NFC chip is not in unmapped_uids file, putting it in there.')
                uidfile.close()
                uidfile = open(file, 'a')
                uidfile.write(uid + '\n')
                uidfile.close()
        except IOError:
            uidfile = open(file, 'w')
            uidfile.write("file starts here" + '\n')
            uidfile.write(uid + '\n')
            uidfile.close()
        return "jan"


# This loop keeps checking for chips. If one is near it will get the UID and change to the according playlist. 
def main():
    # Create an object of the class MFRC522
    MIFAREReader = MFRC522.MFRC522()
    main.uid = "initUid"
    main.uidPrev = "initUidPrev"
    main.mpdStatus = "initState"
    main.errorcount = 0
    # Welcome message
    logging.info('Welcome to musicbox daemon!')
    logging.info('use "systemctl start/stop musicbox" to control this service. ')
    while continue_reading:

        time.sleep(0.4)
        # Scan for cards    
        (status,TagType) = MIFAREReader.MFRC522_Request(MIFAREReader.PICC_REQIDL)
        logging.debug('status after tagType request: ' + str(status) )
        if status != MIFAREReader.MI_OK:
            main.errorcount = main.errorcount + 1
            logging.debug('error count: ' + str(main.errorcount) )
        else:
            main.errorcount = 0
    
        # If a card is found
        if main.errorcount == 0:
            logging.debug('Card detected')
            logging.debug('status before uid request: ' + str(status) )

        # Get the UID of the card
        (status,uid) = MIFAREReader.MFRC522_Anticoll()
        logging.debug('status after uid request: ' + str(status) )
    
        # If we have the UID, continue
        if status == MIFAREReader.MI_OK:
            ## MIFAREReader.MFRC522_SelectTag(uid)
            main.uid =  str(uid[0])+"-"+str(uid[1])+"-"+str(uid[2])+"-"+str(uid[3])
            logging.debug('Card read UID: ' + main.uid)
            logging.debug('status before else statement: ' + str(status) )
            ##mappedPlaylist = "Melissa Etheridge - Brave and crazy" 
            mappedPlaylist = str(mapCardToPlaylist(main.uid))

            if main.uid == main.uidPrev:
                # do nothing
                logging.debug('Nothing changed: Card '+main.uidPrev +' is in place.')
                logging.debug('MPD status: ' + main.mpdStatus )
                if main.mpdStatus != "playing":
                    subprocess.call(["/usr/bin/mpc", "play"])
                    logging.info('Start playing ' + mappedPlaylist)
                    main.mpdStatus = "playing"

            else:
                logging.info('Card has changed: Card ' +main.uidPrev +' has been replaced by '+main.uid+'.')
                # start playing something, defined by the card
                if main.mpdStatus != "cleared":
                    subprocess.call(["/usr/bin/mpc", "clear"])
                    main.mpdStatus = "cleared"
                    logging.info('Clear playlist.')
                    logging.debug('MPD status: ' + main.mpdStatus )
                if main.mpdStatus != "loaded":
                    subprocess.call(["/usr/bin/mpc", "load", mappedPlaylist])
                    main.mpdStatus = "loaded"
                    logging.info('Load playlist.')
                    logging.debug('MPD status: ' + main.mpdStatus )
                if main.mpdStatus != "playing":
                    subprocess.call(["/usr/bin/mpc", "play"])
                    logging.info('Start playing ' + mappedPlaylist)
                    main.mpdStatus = "playing"
                    logging.debug('MPD status: ' + main.mpdStatus )

            main.uidPrev = main.uid 
        
        if main.errorcount == 2:
            logging.debug('No Card can be read...')
            logging.debug('status after else statement: ' + str(status) )
            logging.debug('MPD status: ' + main.mpdStatus )
            # stop playing anything
            if main.mpdStatus != "paused":
                subprocess.call(["/usr/bin/mpc", "pause"])
                logging.info('Paused music player daemon...')
                main.mpdStatus = "paused"
    
    

main()

