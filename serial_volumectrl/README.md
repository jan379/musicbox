
# Just copy over some files and enable this service

chmod 755 serial_volumectrl.py 
cp serial_volumectrl.py /usr/local/bin/
cp serial-volumectrl.service /etc/systemd/system/
systemctl daemon-reload
systemctl enable serial-volumectrl.service 
systemctl start serial-volumectrl.service 
systemctl status serial-volumectrl.service 
journalctl -u serial-volumectrl.service 
adduser mpd dialout
